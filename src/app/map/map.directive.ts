import {Directive, ElementRef, Input, Output, EventEmitter, OnInit} from '@angular/core';
declare let google:any;
declare let jQuery:any;

@Directive({
  selector: '[map]'
})

export class MapDirective implements OnInit {
  @Output() mapClicked:EventEmitter<any> = new EventEmitter();
  @Input() lat:number;
  @Input() lng:number;
  @Input() zoom:number;

  private map:any;
  private marker:any;
  private latitude:number = 40.792903;
  private longitude:number = 43.846497;
  private zoomMap:number = 3;

  ngOnInit() {
    this.latitude = this.lat;
    this.longitude = this.lng;
    this.zoomMap = this.zoom;

    if (this.elementRef.nativeElement.classList.length >= 1) {
      this.map = new google.maps.Map(this.elementRef.nativeElement, {
        center: {lat: this.latitude, lng: this.longitude},
        zoom: this.zoom
      });
      this.marker = new google.maps.Marker({
        position: {lat: this.latitude, lng: this.longitude},
        map: this.map
      });
    }else{
      alert('Error');
    }
    console.log(this.elementRef.nativeElement.classList.length);
    /*
     function mapClick (e){
     window
     }*/
  }

  constructor(private elementRef:ElementRef) {
  }
}


