import { Angular2GoogleMapInputOutputPage } from './app.po';

describe('angular2-google-map-input-output App', function() {
  let page: Angular2GoogleMapInputOutputPage;

  beforeEach(() => {
    page = new Angular2GoogleMapInputOutputPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
